class CreateViewMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :view_messages do |t|

      t.timestamps
    end
  end
end
