class HomesController < ApplicationController
  before_action :set_home, only: [:show, :edit, :update, :destroy]

  def index
    @homes = Home.all
  end

  def show
    @home = Home.find(params[:id])
    @homes = Home.order('created_at desc').limit(4).offset(1)
  end

  def new
    @home = Home.new
  end

  def create
    @home = Home.new(home_params)

    respond_to do |format|
      if @home.save
        format.html do
          flash[:success] = 'Message Sent!'
          redirect_to homes_url
        end
      else
        format.html { flash[:failure!] = 'Message Not Sent! Verify Details & Send Again!' }
        format.html { render 'new' }
      end
    end
  end

  def update
    respond_to do |format|
      if @home.update(home_params)
        format.html { redirect_to @home, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @home }
      else
        format.html { render :edit }
        format.json { render json: @home.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @home.destroy
    respond_to do |format|
      format.html { redirect_to view_messages_url, notice: 'Contact was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    def set_home
      @home = Home.find(params[:id])
    end

    def home_params
      params.require(:home).permit(:name, :email, :message)
    end
end
