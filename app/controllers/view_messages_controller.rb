class ViewMessagesController < ApplicationController
  before_action :set_view_message, only: [:show, :edit, :update, :destroy]

  def index
    @view_messages = ViewMessage.all
    @homes = Home.all
  end

  def show
    @view_message = ViewMessage.find(params[:id])
    @view_messages = ViewMessage.order('created_at desc').limit(4).offset(1)
  end

  def new
    @view_message = ViewMessage.new
  end

  def create
    @view_message = ViewMessage.new(view_message_params)

    respond_to do |format|
      if @view_message.save
        format.html do
          flash[:success] = 'Messages Seen!'
          redirect_to homes_url
        end
      else
        format.html { flash[:failure!] = 'Messages Not Seen! Verify Details & Send Again!' }
        format.html { render 'new' }
      end
    end
  end

  def destroy
    @view_message.destroy
    respond_to do |format|
      format.html { redirect_to view_messages_url, notice: 'Message was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    def set_view_message
      @view_message = ViewMessage.find(params[:id])
    end

    def view_message_params
      params.fetch(:view_message, {})
    end
end
