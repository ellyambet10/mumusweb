json.extract! view_message, :id, :created_at, :updated_at
json.url view_message_url(view_message, format: :json)
