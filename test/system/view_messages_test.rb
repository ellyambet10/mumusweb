require "application_system_test_case"

class ViewMessagesTest < ApplicationSystemTestCase
  setup do
    @view_message = view_messages(:one)
  end

  test "visiting the index" do
    visit view_messages_url
    assert_selector "h1", text: "View Messages"
  end

  test "creating a View message" do
    visit view_messages_url
    click_on "New View Message"

    click_on "Create View message"

    assert_text "View message was successfully created"
    click_on "Back"
  end

  test "updating a View message" do
    visit view_messages_url
    click_on "Edit", match: :first

    click_on "Update View message"

    assert_text "View message was successfully updated"
    click_on "Back"
  end

  test "destroying a View message" do
    visit view_messages_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "View message was successfully destroyed"
  end
end
