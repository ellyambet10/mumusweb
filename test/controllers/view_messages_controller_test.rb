require 'test_helper'

class ViewMessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @view_message = view_messages(:one)
  end

  test "should get index" do
    get view_messages_url
    assert_response :success
  end

  test "should get new" do
    get new_view_message_url
    assert_response :success
  end

  test "should create view_message" do
    assert_difference('ViewMessage.count') do
      post view_messages_url, params: { view_message: {  } }
    end

    assert_redirected_to view_message_url(ViewMessage.last)
  end

  test "should show view_message" do
    get view_message_url(@view_message)
    assert_response :success
  end

  test "should get edit" do
    get edit_view_message_url(@view_message)
    assert_response :success
  end

  test "should update view_message" do
    patch view_message_url(@view_message), params: { view_message: {  } }
    assert_redirected_to view_message_url(@view_message)
  end

  test "should destroy view_message" do
    assert_difference('ViewMessage.count', -1) do
      delete view_message_url(@view_message)
    end

    assert_redirected_to view_messages_url
  end
end
