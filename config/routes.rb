Rails.application.routes.draw do
  resources :view_messages
  resources :homes

  root 'homes#index'
end
